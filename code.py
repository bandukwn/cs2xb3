from collections import Iterable

# Below function converts the 2-D list into a 1-D list
def flatten(lis):
     for item in lis:
         if isinstance(item, Iterable) and not isinstance(item, str):
             for x in flatten(item):
                 yield x
         else:
             yield item

# Below function checks if all students in the list of student numbers
# are in a valid group
def are_valid_groups(student_numbers, groups):
  for group in groups:
    if len(group) not in (2,3):
      return False
  for student in student_numbers:
   if any(student in sublist for sublist in groups) == True and any(list(flatten(groups)).count(element) > 1 for element in list(flatten(groups))) == False:
     pass
   else:
     return False
  return True

# End of code
